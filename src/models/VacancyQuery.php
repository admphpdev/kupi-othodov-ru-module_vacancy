<?php

namespace kupi_othodov_ru\module_vacancy\models;

/**
 * This is the ActiveQuery class for [[Vacancy]].
 *
 * @see Vacancy
 */
class VacancyQuery extends \amd_php_dev\yii2_components\models\SmartQuery
{

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
     * @inheritdoc
     * @return Vacancy[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Vacancy|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

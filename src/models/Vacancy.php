<?php

namespace kupi_othodov_ru\module_vacancy\models;

use amd_php_dev\yii2_components\widgets\form\SmartInput;
use Yii;

/**
 * This is the model class for table "{{%vacancy_vacancy}}".
 *
 * @property string $post
 * @property string $requirements
 * @property string $charge
 * @property integer $id
 * @property integer $active
 * @property integer $priority
 */
class Vacancy extends \amd_php_dev\yii2_components\models\SmartRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vacancy_vacancy}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
        //
        //]);
        return parent::behaviors();
    }

    /**
    * @inheritdoc
    */
    public static function getActiveArray()
    {
        //return \yii\helpers\ArrayHelper::merge(parent::getActiveArray(), [
        //
        //]);
        return parent::getActiveArray();
    }

    /**
    * @inheritdoc
    */
    public function getItemUrl() {
        if ($this->isNewRecord)
            return false;

        //return Url::to(['', 'url' => $this->url]);
        return '';
    }

    /**
    * @inheritdoc
    */
    public function getInputType($attribute)
    {
        $result = null;

        switch ($attribute) {
            case 'requirements' :
                $result = SmartInput::TYPE_TEXTAREA;
                break;
            case 'charge' :
                $result = SmartInput::TYPE_TEXTAREA;
                break;
            default:
                $result = parent::getInputType($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputData($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputData($attribute);
        }

        return $result;
    }

    /**
    * @inheritdoc
    */
    public function getInputOptions($attribute)
    {
        $result = null;

        switch ($attribute) {
            default:
                $result = parent::getInputOptions($attribute);
        }

        return $result;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return \yii\helpers\ArrayHelper::merge(parent::rules(), [
            [['requirements', 'charge'], 'string'],
            [['active', 'priority'], 'integer'],
            [['post'], 'string', 'max' => 255],
        ]);
        /*return [
            [['requirements', 'charge'], 'string'],
            [['active', 'priority'], 'integer'],
            [['post'], 'string', 'max' => 255],
        ];*/
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return \yii\helpers\ArrayHelper::merge(parent::attributeLabels(), [
             'post' => 'Должность',
             'requirements' => 'Требования',
             'charge' => 'Обязанности',
        ]);
    }

    /**
     * @inheritdoc
     * @return VacancyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VacancyQuery(get_called_class());
    }
}

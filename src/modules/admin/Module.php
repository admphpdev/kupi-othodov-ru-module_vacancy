<?php

namespace kupi_othodov_ru\module_vacancy\modules\admin;

/**
 * admin module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Admin
{
    //public $layout      = '@app/views/layouts/default';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'kupi_othodov_ru\module_vacancy\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->modules = [
        //
        //];

        // custom initialization code goes here
    }

    public static function getMenuItems() {
        return [
            'section' => 'admin',
            'items' => [
                [
                    'label' => 'Вакансии',
                    'items' => [
                        ['label' => 'Вакансии', 'url' => ['/vacancy/admin/vacancy/index']],
                    ]
                ]
            ],
        ];
    }
}

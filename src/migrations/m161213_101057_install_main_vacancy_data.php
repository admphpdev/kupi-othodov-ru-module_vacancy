<?php

use amd_php_dev\yii2_components\migrations\generators\Generator;
use amd_php_dev\yii2_components\migrations\Migration;

class m161213_101057_install_main_vacancy_data extends Migration
{
    public static $vacancyTableName = '{{%vacancy_vacancy}}';

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $generator = new Generator($this, self::$vacancyTableName);
        $generator->additionalColumns['post'] = $this->string(255);
        $generator->additionalColumns['requirements'] = $this->text();
        $generator->additionalColumns['charge'] = $this->text();
        $generator->create();
    }

    public function safeDown()
    {
        $this->dropTable(self::$vacancyTableName);
    }
}
